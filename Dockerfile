
FROM ubuntu:latest
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y update
RUN apt-get install -y --no-install-recommends apt-utils build-essential sudo rlwrap r-base

WORKDIR /home/shakti

COPY download/a a
RUN chmod a+rwx a

WORKDIR /home/shakti/a

RUN R CMD SHLIB r.c ./_.so
ENTRYPOINT ["Rscript","r.r"]

