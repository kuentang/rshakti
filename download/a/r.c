// docker build -t rs .
// docker run -t rs
// dyn.load("r.so")
// .Call("oo",list("+" ,5,6))
// R CMD SHLIB r.c -L_.so

#include <errno.h>
#include <R.h>
#include <Rdefines.h>
#include "k.h"

typedef SEXP O;

K ko(O o)S(TYPEOF(o),_(STRSXP,ks( (char*)CHAR(STRING_ELT(o, 0)) ))_(REALSXP,kf(REAL(o)[0] ))_(INTSXP,kj(INTEGER(o)[0])),K x=k(' ',XLENGTH(o));N(xn,xK[i]=ko(VECTOR_ELT(o, i)))x)
O ok(K x)S(tx,_(KS,mkString((S)x))_(KF,ScalarReal(fx))_(KJ,ScalarReal(fx)),O o;PROTECT(o= allocVector(VECSXP,xn));N(xn,SET_VECTOR_ELT(o, i, ok(xK[i])));UNPROTECT(1);o)
O oo(O o){K x=k(ko(o),0);R x<16?error("type"),NULL:(o=ok(x),k(0,x),o);}

